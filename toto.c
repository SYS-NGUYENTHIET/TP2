#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/types.h>
#include  <unistd.h>
#include  <signal.h>

/* q1 */
int q1(void) {
	
	sleep(4);
	printf("%s","au revoir");
	return 0;
}
/* q2 */
int q2(void) {
	pid_t proc;
	proc = fork();
	if ( proc ==  0 ) {
		sleep(4);
		printf("%s","fini");
		/* q5 */
		/* execl("./toto","", NULL); */

	} else {
		sleep(2);
		printf("%s","bye");


		/* q3 */
		/* abort(); */
		/* exit(0); */

		/* q4 */
		/* kill (proc, SIGKILL); */
		return 0;
	}
}

int main(void) {
	/* q1(); */
	q2();
	
}
