#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include  <sys/types.h>
#include  <unistd.h>


void *bonjour(void *p) {
	int cpt = 0;
	
	while (cpt < 10) {
		cpt++;
		sleep(1);
		printf("bonjour");
		fflush(stdout); /* permet de sortir le buffer, sans ceci, il y'a 10 bonjour qui s'affichent en 1 coup au bout de 10 secondes au lieu de 1 fois chaque secondes pendant 10 secondes */
	}
	pthread_exit(0); 
	return NULL;
}

int main(void) {
	pthread_t t;
	pthread_create(&t, NULL, bonjour, NULL);
	pthread_join(t,NULL);

	return 0;
}
