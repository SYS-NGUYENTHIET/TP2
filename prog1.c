#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/types.h>
#include  <unistd.h>

int main(void) {
	pid_t proc;
	proc = fork();
	if ( proc == 0 ) {
		execl("affichez","","salut",NULL);
	}	
	return 0;
}
